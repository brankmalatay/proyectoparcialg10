/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import interfaz.*;
import java.util.*;
import java.time.*;

/**
 *
 * @author Brank
 */
public class Promocion {

    private static Integer codigoIncremental = 1;
    private final Integer codigo;
    private String descripcion;
    private ArrayList<Dia> diasValidez;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private Establecimiento establecimiento;
    private Tarjeta tarjeta;

    public Promocion(String descripcion, ArrayList<Dia> diasValidez, LocalDate fechaInicio, LocalDate fechaFin, Establecimiento establecimiento, Tarjeta tarjeta) {
        this.codigo = codigoIncremental++;
        this.descripcion = descripcion;
        this.diasValidez = diasValidez;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.establecimiento = establecimiento;
        this.tarjeta = tarjeta;
    }
/**
 * Metodo que agrega promociones al establecimiento de una empresa
 * @param empresa Empresa a la que corresponde el establecimieto
 * @param establecimiento Establecimiento al que corresponde la promocion
 **/
    public static void registrarPromocion(Empresa empresa, Establecimiento establecimiento) {
        System.out.print("Ingrese descripción de la promoción : ");
        String nDescripcion = Util.capitalize(Util.ingresoString());
        ArrayList<Dia> nDias = seleccionarDiasValidez();
        System.out.print("Ingrese fecha de inicio de la promoción (DD-MM-AAAA) : ");
        LocalDate nFechaInicio = Util.ingresoFecha();
        while (nFechaInicio == null) {
            System.out.print("Formato inválido. Ingrese la fecha correctamente : ");
            nFechaInicio = Util.ingresoFecha();
        }
        System.out.print("Ingrese fecha de expiración de la promoción (DD-MM-AAAA) : ");
        LocalDate nFechaFin = Util.ingresoFecha();
        while (nFechaFin == null) {
            System.out.print("Formato inválido. Ingrese la fecha correctamente : ");
            nFechaFin = Util.ingresoFecha();
        }
        Tarjeta nTarjeta = Tarjeta.nuevaTarjeta();
        Promocion nPromo = new Promocion(nDescripcion, nDias, nFechaInicio, nFechaFin, establecimiento, nTarjeta);
        establecimiento.getPromociones().add(nPromo);
        System.out.println();
        System.out.println("Promoción agregada con éxito");
        Util.continuar();
    }
/**
 * Metodo que comprueba promociones por descripcion
 * @param nDescripcion La descripcion a consultar
 * @return ArrayList Retorna la promocion segun la descripcion
 **/
    public static ArrayList<Promocion> comprobarPromocionesDescripcion(String nDescripcion) {
        ArrayList<Promocion> resultado = new ArrayList<>();
        for (Empresa e : Data.empresas) {
            for (Establecimiento est : e.getEstablecimientos()) {
                for (Promocion p : est.getPromociones()) {
                    if (p.getDescripcion().toLowerCase().contains(nDescripcion.toLowerCase())) {
                        if (!(resultado.contains(p))) {
                            resultado.add(p);
                        }
                    }
                }
            }
        }
        return resultado;
    }
/**
 * Metodo para comprobar las promociones que hay en uno o varios dias
 * @param nDias Los dias a consultar
 * @return Arraylist Retorna las promociones segun los dias ingresados
 **/
    public static ArrayList<Promocion> comprobarPromocionesDias(ArrayList<Dia> nDias) {
        ArrayList<Promocion> resultado = new ArrayList<>();
        for (Empresa e : Data.empresas) {
            for (Establecimiento est : e.getEstablecimientos()) {
                for (Promocion p : est.getPromociones()) {
                    for (Dia d : nDias) {
                        if (p.getDiasValidez().contains(d)) {
                            if (!(resultado.contains(p))) {
                                resultado.add(p);
                            }
                        }
                    }
                }
            }
        }
        return resultado;
    }
/**
 * Metodo para comprobar las promociones que tiene un tipo de tarjeta
 * @param nTipoTarjeta El tipo de tarjeta a consultar
 * @return Arraylist Retorna las promociones según el tipo de tarjeta
 **/
    public static ArrayList<Promocion> comprobarPromocionesTarjeta(String nTipoTarjeta) {
        ArrayList<Promocion> resultado = new ArrayList<>();
        for (Empresa e : Data.empresas) {
            for (Establecimiento est : e.getEstablecimientos()) {
                for (Promocion p : est.getPromociones()) {
                    if (p.tarjeta.getTipo().equals(nTipoTarjeta)) {
                        if (!(resultado.contains(p))) {
                            resultado.add(p);
                        }
                    }
                }
            }
        }
        return resultado;
    }
/**
 * Metodo para seleccionar los días a consultar
 * @return ArrayList Los dias a consultar
 **/
    public static ArrayList<Dia> seleccionarDiasValidez() {
        ArrayList<Dia> dias = new ArrayList<>();
        boolean seguir = true;
        while (seguir) {
            System.out.println();
            for (Dia d : Dia.values()) {
                System.out.println((d.ordinal() + 1) + ") " + d);
            }
            System.out.print("Ingrese el numero correspondiente al día : ");
            String n = Util.ingresoString();
            while ((!(Util.isNumeric(n))) || (!(Util.isBetween(1, 7, n)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                n = Util.ingresoString();
            }
            Integer N = Integer.parseInt(n);
            Dia nDia = Dia.values()[N - 1];
            dias.add(nDia);
            System.out.print("¿ Desea ingresar otro día ? S/N : ");
            String respuesta = Util.ingresoString();
            while (!((respuesta.equalsIgnoreCase("n")) || (respuesta.equalsIgnoreCase("s")))) {
                System.out.print("¿ Desea ingresar otro día ? S/N : ");
                respuesta = Util.ingresoString();
            }
            switch (respuesta) {
                case "n":
                    seguir = false;
                    break;
                case "N":
                    seguir = false;
                    break;
            }
        }
        return dias;
    }

    /* GETTERS & SETTERS */
    public Integer getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArrayList<Dia> getDiasValidez() {
        return diasValidez;
    }

    public void setDiasValidez(ArrayList<Dia> diasValidez) {
        this.diasValidez = diasValidez;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    @Override
    public String toString() {
        return /*"Empresa : " + establecimiento.getEmpresa().getNombre() + "\n" + "Establecimiento : " + establecimiento.getDireccion() + 
                "\n" + */ "Tarjeta : " + tarjeta + "\n" + "Descripción : " + descripcion + "\n" + "Días de validez : " + diasValidez + "\n" + "Fecha de inicio : "
                + fechaInicio + "\n" + "Fecha de vencimiento : " + fechaFin;
    }
}
