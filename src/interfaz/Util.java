/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/** Declaracion de la clase Util.
 *
 * @author Brank
 * @version 09/07/2019
 */
public class Util {
     /** Metodo ingresoString() Pide por teclado cadenas de caracteres.
     * 
     * @return 
     */
    public static String ingresoString() {
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
    /** Metodo IngresoFecha() Pide por teclado fechas en String y las convierte a tipo Date.
     * 
     * @return 
     */
    public static LocalDate ingresoFecha() {
        Scanner input = new Scanner(System.in);
        String fecha = input.nextLine();
        return toDate(fecha);
    }
    /** Metodo IngresoHora() Pide una hora en String y la convierte a tipo Time.
     * 
     * @return 
     */
    public static LocalTime ingresoHora() {
        Scanner input = new Scanner(System.in);
        String hora = input.nextLine();
        return toTime(hora);
    }
    /**
     * Metodo toDate() Convierte un String a Date.
     * @param s Indica la fecha a convertir.
     * @return 
     */
    public static LocalDate toDate(String s) {
        try {
            DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate horaLocalDate = LocalDate.parse(s, formatoFecha);
            return horaLocalDate;
        } catch (java.time.format.DateTimeParseException e) {
            return null;
        }
    }
    /** Metodo toTime() Convierte un String a Time.
     * 
     * @param s Inica la hora a convertir.
     * @return 
     */
    public static LocalTime toTime(String s) {
        try {
            DateTimeFormatter formatoHora = DateTimeFormatter.ofPattern("HH:mm");
            LocalTime horaLocalTime = LocalTime.parse(s, formatoHora);
            return horaLocalTime;
        } catch (java.time.format.DateTimeParseException e) {
            return null;
        }
    }
    /** Metodo mostrarFecha() Imprime por pantalla una fecha de tipo Date con un formate determinado.
     * 
     * @param fecha Indica la Fecha a imprimir.
     * @return 
     */
    public static String mostrarFecha(LocalDate fecha){
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return fecha.format(formatoFecha);
    }
    /** Metodo isNumeric() Valida si el dato ingresado por teclado es numerico.
     * 
     * @param cadena Indica el numero en String a a validar.
     * @return 
     */
    public static boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException e) {
            resultado = false;
        }
        return resultado;
    }
     /** Metodo isBetween() Permite validar si un valor se enceuntra en un rango determinado.
     * 
     * @param minimo Indica el valor minimo del rango .
     * @param maximo Indica el valor maximo del rango.
     * @param n Indica el numero a validar.
     * @return 
     */
    public static boolean isBetween(int minimo, int maximo, String n) {
        boolean resultado;
        try {
            Integer N = Integer.parseInt(n);
            if ((minimo <= N) && (N <= maximo)) {
                resultado = true;
            } else {
                resultado = false;
            }
        } catch (NumberFormatException e) {
            resultado = false;
        }
        return resultado;
    }
    /** Metodo capitalize() Convierte la primera letra de una cadena de caracteres en mayuscula.
     * 
     * @param s Indica la cadena a convertir.
     * @return 
     */
    public static String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }
    /** Metodo toTitle() Convierte la primera letra de cada oada palabra de una cadena de caracteres en mayusculas.
     * 
     * @param s
     * @return 
     */
    public static String toTitle(String s) {
        String retorno = "";
        String[] palabras = s.split(" ");
        for (String p : palabras) {
            retorno += capitalize(p);
            retorno += " ";
        }
        retorno = retorno.substring(0, retorno.length()-1);
        return retorno;
    }
    /** Metodo limpiarPantalla() Borra todo el contenido que se muestra en ventana.
    * 
    *
    */
    public static void limpiarPantalla() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
        }
    }
    /** Metodo continuar() Permite la visualizacion de la informacion requerida por el usuario, la cual no desaparecera hasta que el usuario presione enter.
     * 
     */
    public static void continuar() {
        System.out.println();
        System.out.println("Presione enter para continuar...");
        ingresoString();
    }

}
